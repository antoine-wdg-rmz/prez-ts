# Why Typescript ?

![Toto](assets/undefined.png)

- catch error at compile-time not at runtime

- types work as documentation

- refactoring MUCH easier

- incredible autocompletion and other editor integration

- easy to opt-out when we want to quickly prototype

---
# Resources 

- [Handbook](http://www.typescriptlang.org/docs/handbook/basic-types.html) (official)

- [Deep dive](https://basarat.gitbooks.io/typescript/docs/types/type-system.html): by a TS dev,
pretty much everything you want to know and some more

- [Relase notes](http://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-0.html) for 2.x (surprisingly insightful)

---
# Primitive types

`number`

```typescript
let x: number = 4;
let y: number = 'toto'; // Error
let z: number = null; // Error
let z: number = undefined; // Error
```

`string`

```typescript
let name: string = 'Toto';
let x: string = '';
```

`boolean`

```typescript
let isLoloBM: boolean = true;
```

---

# Nullish types

```typescript
let nothing: undefined = undefined; 
let alsoNothing: null = null;
```

There are both an `undefined` type and `undefined` value.

Same for `null`.

```typescript
let stillNothing: undefined = null; // Error
```

---

# Functions 

Regular

```typescript
function add(a: number, b: number): number {
  retrun a + b;
}

function addBad(a: number, b: number): number {
  retrun "nope I'm a string"; // Error
} 
```

Arrow

```typescript
const concat = (a: string, b: string): string => a + b;

const concatBis: ((a: string, b: string) => string) = (a, b) => {
  return a + b;
};
```

---

# `void` and `never`

```typescript
function hello(name: string) : void {
  console.log(name);
}


function iThrow() : never {
  throw new Error('Told ya');
}

function infiniteLoop() : never {
  while(true) {}
}

function fakeNever() : never {
  console.log('Nope'); // Error
}
```


---
  
# Type inference 

Variable assignments

```typescript
let a = 4; // : number 
let name = 'Toto'; // : string 
let isLoloBM = true; // : boolean

let b = a + 5; // : number
```

Return types 

```typescript
function hello(name: string) {  // : void
  console.log(name);
}

function add(a: number, b: number) { // : number
  retrun a + b;
}
```

---

# Interface 

```typescript 
interface Point {
  x: number;
  y: number;
}

let p: Point = { x: 4, y: 6 };
let q: Point = { x: 4, y: 'toyo' }; // Error
let r: Point = { x: 4 }; // Error
let s: Point = { x: 3, y: 6, z: 4 }; // Error
let t = { x: 3, y: 6, z: 4 };
let u: Point = t;
```

```typescript
function norm(p: Point) {
  return p.x * p.x + p.y * p.y;
}

let d1 = norm(p);
let d2 = norm({x: 4, y: 6});
let d3 = norm({x: 4, y: 6, z: 4}); // Error
let d4 = norm(t);
```

---

# Interface 

Duck typing

> "If it walks like a duck and it quacks like a duck, then it must be a duck."

```typescript 
interface Point {
  x: number;
  y: number;
}

interface Vector {
  x: number;
  y: number;
}

let a: Point = {x:0, y:1};
let b: Vector = a;
```

---

# Interface 

```typescript
class Stuff implements Point {
  x: number;
  y: number;
  toto: string;
  // constructor omitted
}

let p: Point = new Stuff(3, 4, 'lolo');
```

```typescript
class OtherStuff {
  x: number;
  y: number;
  toto: string;
  // constructor omitted
}

let q: Point = new OtherStuff(3, 4, 'lolo'); // OK
```
---

# Array 

```typescript
let a: number[] = [1, 2, 3];
let b: string[] = ['toto', 'lolo'];

let x = a[0] // : number
```

---



# Soundness 

-  soundness is the ability for a type checker to catch every single error that might happen 
at runtime

- Typescript is **not** sound

```typescript 
let a = [1, 2, 3]; // : number[]
let x = a[0]; // : number (x = 1)
let y = a[5]; // : number (y = undefined)

let z = x + y; // : number (z = NaN)
```

- but it *mostly* is (**with the right compiler options**)

---
# `any`

FUCK TYPESCRIPT, I KNOW BETTER 

```typescript
let a : any = 3;
a.toto = 4; // No compile-time error
let shoopingList: string[] = a; // No compile-time error

function norm(p: Point) {...};

norm(a); // No compile-time error


let b: any = [1, null, 56, 0]; // We can put any type in any
```

- can be assigned anything 

- can be assigned *to* anything

- basicaly disables typescript everywhere this type is used

--> use **ONLY** when prototyping, or in very rare case

---
# Declaration spaces

- **Type declaration space**: type checks done by Typescript 

- **Variable declaration space**: code run by JS (~emitted code)

```typescript 
let y: number = 3; 
// [variable] let y = 3;
// [type] let y: number;
```

```typescript 
let y = 3;
// [variable] let y = 3;
// [type] let y: number;
```

```typescript 
let y = undefined;
// [variable] let y = undefined;
// [type] let y: undefined;
```

```typescript 
let y = {toto: 'lolo', x: 3};
// [variable] let y = {toto: 'lolo', x: 3};
// [type] let y: {toto: string, x: number};
```

---

# Declaration spaces

```typescript
interface Point {x: number; y: number}
// [variable] (nothing, interfaces are type only)
// [type] interface Point {x: number; y: number}
```

---
# Union types

```typescript
let x: number | undefined = 3;
x = undefined;
```

```typescript
const addThree = (a: number | undefined) => {
  return a + 3; // Error 
}
```

---
# Union types

```typescript
const addThreeOrZero = (a: number | undefined) => {
  if(a === undefined) {
    // Here a is of type undefined;
    return 0;
  }
  // Here a is of type number
  return a + 3;
}
```

```typescript
const addThreeOrZeroBis = (a?: number) => { 
  if(typeof a === 'number') {
    // Here a is of type number
    return a + 3;
  }
  // Here a is of type undefined;
  return 0;
}
```

---
# Union types

```typescript
function printName(name: string | (() => string)) {
  let toPrint = typeof name === 'string' ? name : name();
  console.log(toPrint);
}
```

---
# Union types: where it stops being cool

```typescript
interface Named {
  name: string;
}

interface Identified {
  id: number;
}

function greet(greetable: Named | Identified){
  ...
}
```

Interfaces do **not exist** in variable declaration space, 
`typeof greetable === 'Named'` is not an option.

---
# Union types

Not so great solution:

```typescript
interface Named { name: string; }

interface Identified { id: number; }

function greet(greetable: Named | Identified){
  if('name' in greetable) {
     console.log('Hello dear ' + greetable.name);
  } else {
    console.log('Hi n° ' + greetable.number);
  }
}

greet({id: 3, name: null}); // >> Hello dear null
```

- **UNSOUND**
- kind of clunky

--> for a better solution, check out **discriminated unions**

---
# Literal types

```typescript
let name: 'toto' = 'toto';
// [variable] let name = 'toto'
// [type] let name: 'toto'

name = 'lolo' // Error
```

```typescript
let x = 5; // : number
let y : 4 = 4; // : 4
y = x; // Error 
x = y;
```

## Mé sa ser a rien :(

---
# Literal types

Useful with unions

```typescript
function norm(p: Point, normType: 'l1' | 'l2') {
   if(normType === 'l1') {
     return Math.abs(p.x) + Math.abs(p.y);
   }
   return p.x * p.x + p.y * p.y;

}
```

---
# Literal types 

Type inference for `const`:

```typescript
let x = 1; // : number 
const y = 1; // : 1
```

---
# Type aliases

```typescript
interface Point {
  x: number;
  y: number;
}

type Vec2 = Point;
type StringOrNumber = string | number;
```

Type aliases exist only in the **type declaration space**.

---
# Discriminated unions

```typescript
type Greetable = 
  | { isHuman: true ; name: string } 
  | { isHuman: false ; id: number };

function greet(greetable: Greetable) {
  if(greetable.isHuman) {
     console.log('Hello dear ' + greetable.name);
  } else {
    console.log('Hi n° ' + greetable.number);
  }
}
```

---
# Discriminated unions: handling failure

```typescript
type Success = { success: true; value: number }; 
type Failure = { success: false; errorMsg: string };
type Result = Success | Failure;

function readNumberFromFile() : Result {...} 

let result = readNumberFromFile();
if(result.success) {
  console.log('Read: ' + result.value;
} else {
  console.log('There was an error ' + result.errorMsg);
}
```


